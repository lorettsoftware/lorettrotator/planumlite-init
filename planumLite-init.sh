# 
# PlanumLite software installer v0.1.22082022
# 30.06.2022
# 22.08.2022
#
# Created by MrFentazis
#

cd ~
mkdir lorett

cd lorett
mkdir log data dependencies low upper


sudo apt update -y

sudo apt upgrade -y

# for support wireless connection

sudo apt install network-manager -y

sudo nmcli r wifi on

sudo nmcli dev wifi

sudo nmcli dev wifi connect Lorett-MikroTik password 0987654321


# for hotspot wifi
sudo apt install net-tools -y
# wlxa0a3f0d19e5e
# https://habr.com/ru/post/188274/

export HOST=hostname
nmcli dev set wlp2s0 managed no

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd

#


sudo apt install python3-distutils python3-scipy python3-matplotlib python3-numpy -y
sudo apt install git curl wget jq -y
#sudo apt-get install i2c-tools -y

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

sudo python3 get-pip.py
python3 get-pip.py

rm get-pip.py

git clone https://gitlab.com/lorettsoftware/lorettrotator/planumliteupper.git
git clone https://gitlab.com/lorettsoftware/lorettrotator/planumlitelow.git
git clone https://gitlab.com/lpmrfentazis/HRPTAutoDecoder.git

mv planumliteupper/* upper
rm -rf planumliteupper

mv planumlitelow/* low
rm -rf planumlitelow

cd HRPTAutoDecoder


# Edit HRPTAutoDecoder config
jq -r '.satDumpPath = "/home/planum/lorett/satdump/build" | .dataPath = ["/home/planum/lorett/data"] | .keepBadIQ = false | .keepGoodIQ = false' config.json > temp.json
rm config.json
mv temp.json config.json

cd ..

wget https://raw.githubusercontent.com/platformio/platformio-core-installer/master/get-platformio.py -O get-platformio.py
python3 get-platformio.py
rm get-platformio.py

sudo echo 'export PATH="/$HOME/.platformio/penv/bin/":$PATH' >> ~/.bashrc

curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules | sudo tee /etc/udev/rules.d/99-platformio-udev.rules

sudo service udev restart

sudo usermod -a -G dialout $USER
sudo usermod -a -G plugdev $USER

# Install python libs
sudo python3 -m pip install lorettOrbital coloredlogs pyserial docopt verboselogs
python3 -m pip install lorettOrbital coloredlogs pyserial docopt verboselogs


#pip install mpu9250-jmdev


# Install SatDump
# Linux: Install dependencies
sudo apt install build-essential cmake make g++ gcc pkgconf libfftw3-dev libvolk2-dev libjpeg-dev libpng-dev -y # Core dependencies. If libvolk2-dev is not available, use libvolk1-dev
sudo apt install libnng-dev -y                                                                             # If this packages is not found, follow build instructions below for NNG
sudo apt install libairspy-dev libairspyhf-dev -y                             # All libraries required for live processing (optional)

# fix missing openGL (Yes, even if select without GUI version)
sudo apt-get install libglfw3 -y
sudo apt-get install libglfw3-dev libglew-dev -y
sudo apt-get install libgl1-mesa-dev -y

# Finally, SatDump
git clone https://github.com/altillimity/satdump.git
cd satdump
mkdir build && cd build

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr .. -DNO_GUI=ON # Linux
make -j4
ln -s ../pipelines . # Symlink pipelines so it can run
ln -s ../resources . # Symlink pipelines so it can run
ln -s ../Ro* . # Symlink fonts for the GUI version so it can run

# End install SatDump

cd ~/lorett

# Install SoapySDR and Enveroment
# install dependencies

# core framework and toolkits (required)
sudo add-apt-repository -y ppa:pothosware/framework

# support libraries for pothos (required)
sudo add-apt-repository -y ppa:pothosware/support

# supplies soapysdr, and drivers (optional)
sudo add-apt-repository -y ppa:myriadrf/drivers

# needed when using usrp devices
sudo add-apt-repository -y ppa:ettusresearch/uhd

# always update after adding PPAs
sudo apt-get update -y

# soapy sdr runtime and utilities
apt-get install soapysdr-tools -y
sudo apt-get install soapysdr-tools -y

# python3 language bindings
apt-get install python3-soapysdr -y
sudo apt-get install python3-soapysdr -y

# End install SoapySDR

#print information about the install
SoapySDRUtil --info
SoapySDRUtil --probe="driver=airspy"


sudo apt-get install ntp
sudo systemctl enable ntp || update-rc.d ntp defaults
sudo systemctl start ntp || service ntp start

sudo systemctl disable systemd-networkd-wait-online.service
sudo systemctl mask systemd-networkd-wait-online.service
#service ntp restart
#date -s '2014-12-25 12:34:56'

sudo apt remove -y nodejs
sudo apt autoremove -y
sudo apt clean -y

cd ~
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh

sudo bash nodesource_setup.sh
sudo apt install nodejs
sudo apt install -y npm
sudo npm i cloudcmd -g --force

export cloudPath="/etc/systemd/system/cloudCommander.service"
tempPath="t.service"
echo "[Unit]" > $tempPath
echo "Description = CloudExplorer for lorett planumLite station" >> $tempPath
echo "" >> $tempPath
echo "[Service]" >> $tempPath
echo 'ExecStart=sudo cloudcmd --port 80 --online --prefix "exp" --root "/home/planum/"' >> $tempPath
echo "Restart=always" >> $tempPath
echo "WorkingDirectory=/home/planum/" >> $tempPath
echo "Type=simple" >> $tempPath
echo "" >> $tempPath
echo "[Install]" >> $tempPath
echo "WantedBy=multi-user.target" >> $tempPath
sudo mv $tempPath $cloudPath

sudo systemctl enable cloudCommander.service
sudo systemctl start cloudCommander.service



#sudo cloudcmd --port 80 --online --prefix "exp" --root "/home/planum/" &
